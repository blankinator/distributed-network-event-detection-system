import numpy as np


def interpret_prediction_result(result):
    return np.max(result)
