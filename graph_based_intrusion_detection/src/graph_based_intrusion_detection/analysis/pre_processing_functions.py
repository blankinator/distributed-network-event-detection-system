

def extract_node_connections(node, edges):
    """
    Extract the connections of a node from a list of edges
    :param node: node identifier
    :param edges: list of edges
    :return: list of connections
    """
    connections = list()
    for edge in edges:
        if edge[0] == node:
            connections.append(edge[1])
        else:
            connections.append(edge[0])

    return connections