from graph_based_intrusion_detection.utils.constants import LOG_FILE_PATH, LOG_LEVEL

import logging


def create_logger(name: str,
                  log_file: str = LOG_FILE_PATH,
                  log_level: str = LOG_LEVEL,
                  ) -> logging.Logger:
    """
    Create a logger for a thread
    :param name: name of the logger
    :param log_file: path to the log file
    :param log_level: log level
    :return: logger
    """
    logger = logging.getLogger(name)
    logger.setLevel(eval(f"logging.{log_level}"))
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add stream handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    ch.setFormatter(formatter)

    # add file handler
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger
