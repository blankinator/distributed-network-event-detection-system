import os
from functools import partial

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

from graph_based_intrusion_detection.packet_processing.graph_processing import update_layer_3_graph, \
    update_layer_2_graph
from graph_based_intrusion_detection.packet_processing.packet_inspection import update_port_list


LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")
LOG_LEVEL = os.getenv("LOG_LEVEL")

# packet processing
PACKET_CONSUMER_BATCH_SIZE = 10_000
PACKET_POLL_TIMEOUT_MS = 100
PACKET_TOPIC_NAME = "packets"
PROCESSED_STATE_TOPIC_NAME = "processed_states"

# state merging
STATE_MERGE_TIMEOUT_MS = 100
STATE_POLL_TIMEOUT_MS = 100
STATE_LOCK_NAME = "network_state_lock"
NETWORK_STATE_KEY = "network_state"
STATE_CONSUMER_BATCH_SIZE = 10

# work dispatcher
NODE_ANALYSIS_TOPIC = "node_analysis"
NODE_ANALYSIS_RESULTS_TOPIC = "node_analysis_results"
NODE_ANALYSIS_RESULTS_POLL_TIMEOUT_MS = 100
NODE_ANALYSIS_INTERVAL_MS = 1000
NETWORK_STATE_CONNECTION_GRAPH_KEY = "layer_3_graph"
NODE_REGISTRY_KEY = "connection_registry"
NODE_REGISTRY_LOCK = "connection_registry_lock"

# connection analysis
NODE_ANALYSIS_POLL_TIMEOUT_MS = 100
NODE_ANALYSIS_BATCH_SIZE = 100
NODE_ANALYSIS_TIMEOUT_MS = 10_000
SURPRESS_WARNINGS = True

TIME_FIELD = 'geninfo/timestamp'
TIMEDELTA_FIELD = "timedelta"
