from datetime import datetime, timedelta
import json
import pickle

import networkx as nx
from networkx.readwrite import json_graph
import pandas as pd


def serialize_series(series: pd.Series) -> bytes:
    """
    Serialize a pandas series to a dictionary containing the data and the dtype
    :param series: series to serialize
    :return: dictionary containing the data and the dtype
    """

    return series.to_parquet()


def deserialize_series(series_dict: dict) -> pd.Series:
    """
    Deserialize a dictionary to a pandas series
    :param series_dict: dictionary containing the data and the dtype
    :return: pandas series
    """

    return pd.Series(series_dict["data"]).astype(series_dict["dtype"])


def serialize_graph(graph: nx.Graph) -> dict:
    """
    Serialize a networkx graph to a dictionary containing the data and the dtype
    :param graph: graph to serialize
    :return: dictionary containing the data and the dtype
    """

    return json_graph.node_link_data(graph)


def deserialize_graph(graph_dict: dict) -> nx.Graph:
    """
    Deserialize a dictionary to a networkx graph
    :param graph_dict: dictionary containing the data and the dtype
    :return: networkx graph
    """

    return json_graph.node_link_graph(graph_dict)


def serialize(value) -> bytes:
    """
    Serialize a value by using pickle
    :param value: value to serialize
    :return: serialized value as bytes
    """

    return pickle.dumps(value)


def deserialize(value: bytes):
    """
    Deserialize a value by using pickle
    :param value: value to deserialize
    :return: deserialized value
    """
    return pickle.loads(value)

# def serialize(value):
#     """
#     Serialize a value
#     :param value: any value
#     :return: serialized value
#     """
#     if isinstance(value, nx.Graph):
#         return serialize_graph(value)
#     elif isinstance(value, dict):
#         return {k: serialize(v) for k, v in value.items()}
#     elif isinstance(value, list) or isinstance(value, tuple):
#         return [serialize(v) for v in value]
#     elif isinstance(value, pd.DataFrame):
#         return serialize(value.to_dict())
#     elif isinstance(value, datetime):
#         return value.isoformat()
#     elif isinstance(value, timedelta):
#         return value.total_seconds()
#     else:
#         return value
