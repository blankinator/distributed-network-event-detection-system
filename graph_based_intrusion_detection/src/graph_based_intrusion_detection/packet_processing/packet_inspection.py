import pandas as pd


def update_port_list(packet: dict | pd.Series, current_port_list: list, port_field: str) -> list:
    if pd.isna(packet[port_field]):
        return current_port_list
    else:
        port = int(packet[port_field])
        if port not in current_port_list:
            current_port_list.append(port)
        return current_port_list
