# Amazon Echo Dot (3rd Gen)
- Amazon Alexa App
- IP: 192.168.1.203
- MAC: 34:25:be:ef:91:bf

## Protokolle:
- Alles IP4
    - TCP
    - TLS v1.2
    - TLS v1
    - ICMP
    - DNS
    - MDNS  / local dns


## Source Ports:
- Random

## Destinations: 
Viele verschiedene IP Adressen, die zu Amazon gehören.
Über Port 443, also HTTPS.

## Kommunikation:
- Periodische, aber nicht regelmäßige Kommunikation mit Amazon Servern
- Kommunikation mit Amazon Servern, wenn Kommando gegeben wird
- Durgängig Kommunikation mit Amazon Servern, wenn Musik abgespielt wird

## Angriffe
- Scan T0 (Pakete 49-262255)
    - Try to scan range of open TCP Ports
    - nmap -sV -p 1-65535 192.168.1.203
    - Open Ports:
        - 1080/tcp open socks  
        - 4070/tcp open tripe  
        - 6543/tcp open mythtv  
        - 7805/tcp open unknown  
        - 8888/tcp open sun-answerbook  
        - 10001/tcp open scp-config  
        - 55442/tcp open unknown  
        - 55443/tcp open unknown  
  
- TELNET Exploit Attempt (Packets 624338 - 624363)

- DoS (Packets 624541-627011)
    - Exhaustion Attack with slow DoS
    - sudo hping3 -S -p 80 --fast 192.168.1.203

## Aktivitäten
- 2330 - 12198: music listening
- 12744-13396: announcement
- 13548 - 20533: key exchange? is this activity?
- 20556 - 26757: music
- 26813 - 26825: announcement
- 26918 - 26936: volume adjustment
